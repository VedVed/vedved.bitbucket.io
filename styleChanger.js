defaultStyle = () => {
  document.getElementById("sonya").style.filter = "";
  document.getElementById("textholder").innerHTML = "";
}

styleSepia = (sepiaPercent) => {
  document.getElementById("sonya").style.filter = sepiaPercent;
  document.getElementById("textholder").innerHTML = "Sepia is a reddish-brown color named after the rich brown pigment of the cuttlefish <a href='https://en.wikipedia.org/wiki/Sepia_(genus)' target='_blank'>Sepia</a>." + "<br>" + "0% sepia is the original image" + "<br>" + "100% converts the image to sepia completely" + "<br>" + "Values over 100% have the same effect as 100%.";
}

let grayscaleDefiniton = "Grayscale conversion:" + "<br>" + "0% = colors unchanged" + "<br>" + "100% = black and white image" + "<br>" + "Values over 100% have the same effect as 100%.";
styleGrayscale = (grayPercent) => {
  document.getElementById("sonya").style.filter = grayPercent;
  document.getElementById("textholder").innerHTML = grayscaleDefiniton;
}

let saturateDefinition = "Saturation is a \"colorfulness\" of an area. Saturation is also the S part of the <a href='https://www.w3schools.com/colors/colors_hsl.asp' target='_blank'>HSL model</a>." + "<br>" + "0% is a completely unsaturated image (grayscale)," + "<br>" + "100% is an original image," + "<br>" + "\> 100% = high saturation";
styleSaturate = (saturatePercent) => {
  document.getElementById("sonya").style.filter = saturatePercent;
  document.getElementById("textholder").innerHTML = saturateDefinition;
}


styleHueRotate = (hueDegree) => {
  document.getElementById("sonya").style.filter = hueDegree;
  document.getElementById("textholder").innerHTML = "Hue is an angular position (0—360 degrees) on a colorspace coordinate diagram (color wheel).";
}

styleInvert = (invertPercent) => {
  document.getElementById("sonya").style.filter = invertPercent;
  document.getElementById("textholder").innerHTML = "The invert function makes dark areas bright and bright areas dark." + "<br>" + "0% leaves the image unchanged" + "<br>" + "100% = completely inverted image that is similar to a photographic negative.";
}

styleContrast = (contrastPercent) => {
  document.getElementById("sonya").style.filter = contrastPercent;
  document.getElementById("textholder").innerHTML = "Contrast is the difference in the color and brightness of the object and other objects within the same field of view." + "<br>" + "0% = completely gray," + "<br>" + " 100% = unchanged,"+"<br>"+"200% = high contrast";
}

styleBlur = (blurPixel) => {
  document.getElementById("sonya").style.filter = blurPixel;
  document.getElementById("textholder").innerHTML = "Blur is the loss of focus. The value defines how many pixels on the screen blend into each other (a larger value creates more blur).";
}

//Arrows to slide images
let n = 0;
let imageNames = ["url('images/coast.JPG')", "url('images/flower.JPG')", "url('images/lighthouse.JPG')", "url('images/cat.JPG')"];

animateImage = (addedClass) =>{
  document.getElementById("sonya").classList.add(addedClass);
  setInterval(function(){ document.getElementById("sonya").classList.remove(addedClass); }, 5000);
}

arrowRight = () => {
  if (n == imageNames.length - 1) { n = 0 }
  else {n++};
  document.getElementById("sonya").style.backgroundImage = imageNames[n];
  document.getElementById("sonya").textContent = `${n+1}/${imageNames.length}`;
}
window.addEventListener("keydown", function (event) {
  switch (event.key) {
  case "ArrowRight":
  		arrowRight();
  		break;
  case "ArrowLeft":
  		arrowLeft();
  		break;
  }
})

arrowLeft = () => {
  if (n == 0) { n = imageNames.length - 1 }
  else {n--};
  document.getElementById("sonya").style.backgroundImage = imageNames[n];
  document.getElementById("sonya").textContent = `${n+1}/${imageNames.length}`;
}

document.getElementById("countrySelector").addEventListener("click", displayCountries);
let y = document.getElementsByClassName("countries");
function displayCountries() {
	let switcher = document.getElementById("countries");
	let switcherButton = document.getElementById("countrySelector");
	if (switcher.style.display == "none") {
	    switcher.style.display = "block";
	  //  switcherButton.classList.add("buttonpressed");
	}
	else { 	
	switcher.style.display = "none";
	//switcherButton.classList.remove("buttonpressed");
	}
}

document.getElementById("crete").addEventListener("click", displayCrete);

function displayCrete() {
	imageNames = ["url('images/coast.JPG')", "url('images/flower.JPG')", "url('images/lighthouse.JPG')", "url('images/cat.JPG')"];
	document.getElementById("sonya").style.backgroundImage = imageNames[0];
	document.getElementById("countries").style.display = "none";
}

document.getElementById("montenegro").addEventListener("click", displayMontenegro);

function displayMontenegro() {
	imageNames = ["url('images/green.JPG')", "url('images/kotor.JPG')", "url('images/cats.JPG')", "url('images/river.JPG')", "url('images/sea.JPG')"];
	document.getElementById("sonya").style.backgroundImage = imageNames[0];
	document.getElementById("countries").style.display = "none";
}



